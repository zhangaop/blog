package export;

import com.zl.entity.po.t_User;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author zhanglei2
 * @since 2020/3/31 18:48
 */
public class TestExcel {

    public static void main(String[] args) throws Exception {
        t_User user0 = new t_User();
        user0.setId((long) 1);
        user0.setNickname("自己");
        user0.setPassword("1223");
        t_User user1 = new t_User();
        user1.setId((long) 1);
        user1.setNickname("自己");
        user1.setPassword("1223");
        t_User user2 = new t_User();
        user2.setId((long) 1);
        user2.setNickname("自己");
        user2.setPassword("1223");
        t_User user3 = new t_User();
        user3.setId((long) 1);
        user3.setNickname("自己");
        user3.setPassword("1223");
        t_User user4 = new t_User();
        user4.setId((long) 1);
        user4.setNickname("自己");
        user4.setPassword("1223");
        t_User user5 = new t_User();
        user5.setId((long) 1);
        user5.setNickname("自己");
        user5.setPassword("1223");
        t_User user6 = new t_User();
        user6.setId((long) 1);
        user6.setNickname("自己");
        user6.setPassword("1223");
        t_User user7 = new t_User();
        user7.setId((long) 1);
        user7.setNickname("自己");
        user7.setPassword("1223");
        ExcelUtils excelUtils = new ExcelUtils();
        List<t_User> list = new ArrayList<>();
        list.add(user0);
        list.add(user1);
        list.add(user2);
        list.add(user3);
        list.add(user4);
        list.add(user5);
        list.add(user6);
        list.add(user7);
        Map<String,String> map = new LinkedHashMap<>();
        map.put("id","序号");
        map.put("nickname","昵称");
        map.put("password","密码");
        OutputStream out = new FileOutputStream("D:\\test.xls");
        ExcelUtils.listToExcel(list,out,map);

    }

}
