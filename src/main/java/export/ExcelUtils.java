package export;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.io.OutputStream;
import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;

/**
 * @author zhanglei2
 * @since 2020/3/31 14:58
 */
public class ExcelUtils {
    private final static int sheetSize=500;//单个sheet存储的数据
    /**
     * @Title: listToExcel
     * @Description: 将列表数据导出到Excel
     * @param data 要导出的数据
     * @param out excel的输出目的地,通过输出流
     * @param fields 每个属性对应的中文名
     * @return void 返回类型
     */
    public static <T> void listToExcel(List<T> data, OutputStream out, Map<String,String> fields) throws Exception {

        /**
         * 导出的步骤
         * 1.创建excel工作簿
         * 2.创建sheet
         * 3.创建行row
         * 4.创建列 cel
         * 5.填充数据
         */

    //    xls格式的excle不能存储太多数据,最多65536
        HSSFWorkbook workbook = new HSSFWorkbook();
        // 计算一共有多少个sheet
        int sheetNum = data.size()/sheetSize;
        if(data.size()%sheetSize!=0){
            sheetNum+=1;
        }
        //设置属性对应名称
        String[] fieldNames = new String[fields.size()];
        String[] chinaNames = new String[fields.size()];
        int count = 0;
        //拿出属性
        for (Map.Entry<String, String> entry : fields.entrySet()) {
            String fieldName = entry.getKey();
            String chinaName = entry.getValue();
            fieldNames[count] = fieldName;
            chinaNames[count] = chinaName;
            count++;
        }
        //填充数据
        for(int i=0;i<sheetNum;i++){

            int rowCount = 0;//行
            //创建sheet
            HSSFSheet sheet = workbook.createSheet();
            //起始索引
            int startIndex = i*sheetSize;
            //结束索引
            int endIndex = (i+1)*sheetSize-1>data.size()?data.size():(i+1)*sheetSize-1;
            //创建行
            HSSFRow row = sheet.createRow(rowCount);
            //标题行 第一行
            for(int j = 0;j<chinaNames.length;j++){
                HSSFCell cell = row.createCell(j);//设置列
                cell.setCellValue(chinaNames[j]);//设置每列的中文标题
            }
            rowCount++;
            for(int index = startIndex;index<endIndex;index++){
                //获取实体类的对象
                T item = data.get(index);
                //创建行
                row=sheet.createRow(rowCount);
                //遍历列
                for(int j = 0;j<chinaNames.length;j++){
                    //获取对象,通过map中的字段名(英文),反射获取该字段的值
                    Field field = item.getClass().getDeclaredField(fieldNames[j]);
                    //允许获取get和set方法
                    field.setAccessible(true);
                    //获取对象的某一列
                    Object o = field.get(item);
                    String value = o==null?"":o.toString();
                    HSSFCell cell = row.createCell(j);//设置列
                    cell.setCellValue(value);//设置中文标题
                }
                rowCount++;
                //通过反射取值

            }

            workbook.write(out);
        }
    }
}
