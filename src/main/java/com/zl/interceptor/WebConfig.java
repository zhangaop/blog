package com.zl.interceptor;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Created by limi on 2017/10/15.
 */
@Configuration
public class WebConfig extends WebMvcConfigurerAdapter {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new LoginInterceptor())
                .addPathPatterns("/admin/**")
                .excludePathPatterns("/admin")
                .excludePathPatterns("/admin/getUserNum")
                .excludePathPatterns("/admin/sendPassword")
                .excludePathPatterns("/admin/userReg")
                .excludePathPatterns("/admin/login");
    }

    /**
     * 放行静态资源
     * @param registry
     */
    // @Override
    // public void addResourceHandlers(ResourceHandlerRegistry registry) {
    //     // registry.addResourceHandler("/static/**")
    //     //         .addResourceLocations("classpath:/static/");
    // }
}
