package com.zl.controller.admin;

import com.zl.entity.povo.Blog;
import com.zl.entity.povo.User;
import com.zl.service.BlogService;
import com.zl.service.TagService;
import com.zl.service.TypeService;
import com.zl.vo.BlogQuery;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;

@Log4j
@Controller
@RequestMapping("/admin")
public class BlogController {

    private static final String INPUT = "admin/blogs-input";
    private static final String LIST = "admin/blogs";
    private static final String REDIRECT_LIST = "redirect:/admin/blogs";


    @Autowired
    private BlogService blogService;
    @Autowired
    private TypeService typeService;
    @Autowired
    private TagService tagService;

    /**
     * 获取后台博客列表
     * @param pageable
     * @param blog
     * @param model
     * @PageableDefault(size = 8, sort = {"updateTime"}, direction = Sort.Direction.DESC)
     * @return
     */
    @GetMapping("/blogs")
    public String blogs( @RequestParam(value = "pageNum",defaultValue = "1") Integer pageNum,
                        BlogQuery blog, Model model,HttpSession session) {
        model.addAttribute("types", typeService.listType());
        log.info("===========开始");
        User user = (User) session.getAttribute("user");
        model.addAttribute("page", blogService.listBlog(pageNum,blog, user));
        log.info("===========结束");

        return LIST;
    }

    /**
     * @PageableDefault(size = 8, sort = {"updateTime"}, direction = Sort.Direction.DESC)
     * @param pageable
     * @param blog
     * @param model
     * @return
     */
    @PostMapping("/blogs/search")
    public String search( @RequestParam(value = "pageNum",defaultValue = "1") Integer pageNum,
                         BlogQuery blog, Model model ,HttpSession session) {
        User user = (User) session.getAttribute("user");
        model.addAttribute("page", blogService.listBlog(pageNum, blog,user));
        return "admin/blogs :: blogList";
    }

    /**
     * 进入博客编辑界面
     * @param model
     * @return
     */
    @GetMapping("/blogs/input")
    public String input(Model model) {
        setTypeAndTag(model);
        model.addAttribute("blog", new Blog());
        return INPUT;
    }

    private void setTypeAndTag(Model model) {
        model.addAttribute("types", typeService.listType());
        model.addAttribute("tags", tagService.listTag());
    }

    /**
     * 获取博客的数据编辑博客
     * @param id
     * @param model
     * @return
     */
    @GetMapping("/blogs/{id}/input")
    public String editInput(@PathVariable Long id, Model model) {
        setTypeAndTag(model);
        log.info("编辑博客获取博客数据==========");
        Blog blog = blogService.getBlog(id);
        blog.init();
        model.addAttribute("blog",blog);
        log.info("jieshu ======================");
        return INPUT;
    }


    /**
     * 保存发布博客
     * @param blog
     * @param attributes
     * @param session
     * @return
     */
    @PostMapping("/blogs")
    public String post(Blog blog, RedirectAttributes attributes, HttpSession session) {
        blog.setUser((User) session.getAttribute("user"));
        blog.setType(typeService.getType(blog.getType().getId()));
        blog.setTags(tagService.listTag(blog.getTagIds()));
        Blog b;
        if (blog.getId() == null) {
            b =  blogService.saveBlog(blog);
        } else {
            b = blogService.updateBlog(blog.getId(), blog);
        }

        if (b == null ) {
            attributes.addFlashAttribute("message", "操作失败");
        } else {
            attributes.addFlashAttribute("message", "操作成功");
        }
        return REDIRECT_LIST;
    }

    /**
     * 删除博客文章mybatis改造
     * @param id
     * @param attributes
     * @return
     */
    @GetMapping("/blogs/{id}/delete")
    public String delete(@PathVariable Long id,RedirectAttributes attributes) {
        blogService.deleteBlog(id);
        attributes.addFlashAttribute("message", "删除成功");
        return REDIRECT_LIST;
    }



}
