package com.zl.controller;

import com.zl.entity.povo.Tag;
import com.zl.service.BlogService;
import com.zl.service.TagService;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;


@Log4j
@Controller
public class TagShowController {

    @Autowired
    private TagService tagService;

    @Autowired
    private BlogService blogService;

    @GetMapping("/tags/{id}")
    public String tags(@RequestParam(value = "pageNum",defaultValue = "1") Integer pageNum,
                        @PathVariable Long id, Model model) {
        log.error("标签获取博客列表===================================");
        List<Tag> tags = tagService.listTagTop(10000);
        log.error("标签========================");
        if (id == -1) {
           id = tags.get(0).getId();
        }
        model.addAttribute("tags", tags);
        log.error("标签获取博客列表");
        model.addAttribute("page", blogService.listBlog(id,pageNum));
        log.error("标签获取博客列表===================================");

        model.addAttribute("activeTagId", id);
        return "tags";
    }
}
