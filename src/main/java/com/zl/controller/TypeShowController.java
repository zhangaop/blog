package com.zl.controller;

import com.zl.entity.povo.Type;
import com.zl.entity.povo.User;
import com.zl.service.BlogService;
import com.zl.service.TypeService;
import com.zl.vo.BlogQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;


@Controller
public class TypeShowController {

    @Autowired
    private TypeService typeService;

    @Autowired
    private BlogService blogService;

    @GetMapping("/types/{id}")
    public String types(@RequestParam(value = "pageNum",defaultValue = "1") Integer pageNum,
                        @PathVariable Long id, Model model) {
        List<Type> types = typeService.listTypeTop(10000);
        if (id == -1) {
           id = types.get(0).getId();
        }
        BlogQuery blogQuery = new BlogQuery();
        blogQuery.setTypeId(id);
        User user = new User();
        model.addAttribute("types", types);
        model.addAttribute("page", blogService.listBlog(pageNum,id));
        model.addAttribute("activeTypeId", id);
        return "types";
    }
}
