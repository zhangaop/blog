package com.zl.util;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;
/**
 * @author zhanglei2
 * @since 2020/3/12 20:36
 */
public class EmailUtil {



        public static void sendMail(String email, String emailMsg)
                throws AddressException, MessagingException {
            // 1.创建一个程序与邮件服务器会话对象 Session

            Properties props = new Properties();
            //设置发送的协议
            props.setProperty("mail.transport.protocol", "SMTP");

            //设置发送邮件的服务器
		    props.setProperty("mail.host", "smtp.163.com");
            //本地邮箱服务器
            // props.setProperty("mail.host", "localhost");
            props.setProperty("mail.smtp.auth", "true");// 指定验证为true
            // props.setProperty("mail.smtp.auth", "true");//开启认证
            props.setProperty("mail.debug", "true");//启用调试
            props.setProperty("mail.smtp.timeout", "1000");//设置链接超时
            props.setProperty("mail.smtp.port", "465");//设置端口
            props.setProperty("mail.smtp.socketFactory.port", "465");//设置ssl端口
            props.setProperty("mail.smtp.socketFactory.fallback", "false");
            props.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");

            // 创建验证器
            Authenticator auth = new Authenticator() {
                @Override
                public PasswordAuthentication getPasswordAuthentication() {
                    //设置发送人的帐号和密码
                    return new PasswordAuthentication("zl1136776278@163.com", "zl561129");
                }
            };

            Session session = Session.getInstance(props, auth);

            // 2.创建一个Message，它相当于是邮件内容
            Message message = new MimeMessage(session);

            //设置发送者
            message.setFrom(new InternetAddress("zl1136776278@163.com"));

            //设置发送方式与接收者
            message.setRecipient(RecipientType.TO, new InternetAddress(email));

            //设置邮件主题
            message.setSubject("个人博客找回密码");
            // message.setText("这是一封激活邮件，请<a href='#'>点击</a>");

            // String url="http://localhost:8080/ShopStore/UserServlet?method=active&code="+emailMsg;
            // String content="<h1>来自购物天堂的激活邮件!激活请点击以下链接!</h1><h3>请<a href='"+url+"'>点击激活</a></h3>";
            String content="<h1>来自您的个人博客的找回密码邮件!</h1><h3>您的密码是:<span style='color:red'>"+emailMsg+"</span></h3>";
            //设置邮件内容
            message.setContent(content, "text/html;charset=utf-8");

            // 3.创建 Transport用于将邮件发送
            Transport.send(message);
        }
    // public static void main(String[] args) throws AddressException, MessagingException {
    //     EmailUtil.sendMail("1136776278@qq.com", "张磊邮件测试:123456");
    // //    liboware@163.com
    // }


}
