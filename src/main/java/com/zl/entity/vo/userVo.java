package com.zl.entity.vo;

import lombok.Data;

/**
 * @author zhanglei2
 * @since 2020/3/12 19:27
 */
@Data
public class userVo {
    private String email;

    private String username;

    private String password;
}
