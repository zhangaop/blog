package com.zl.entity.dto;

/**
 * @author zhanglei2
 * @since 2020/3/10 15:28
 */
import com.zl.entity.po.t_User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Singular;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

// @RequiredArgsConstructor
@Data
@Builder
public class CommonResponseDto implements Serializable {

    private Object data;

    // @Singular
    // private List<?> dataLists;

    private boolean success;

    private boolean failed;

    private Integer code;


    // public CommonResponseDto data(Object data){
    //     this.data = data;
    //     return this;
    // }
    // public CommonResponseDto data(Object data,Integer code){
    //     CommonResponseDto dto = new CommonResponseDto();
    //     /*this.data = data;
    //     this.code = code;*/
    //     dto.setData(data);
    //     dto.setCode(code);
    //     return dto;
    // }
    //
    // public CommonResponseDto success(boolean success){
    //     this.success = success;
    //     return this;
    // }
    // public CommonResponseDto failed(boolean failed){
    //     this.failed = failed;
    //     return this;
    // }
}