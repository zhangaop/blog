package com.zl.entity.po;

import lombok.Data;

/**
* @author zhanglei2
* @since 2020/3/22 12:36
*/
@Data
public class TAutho {
    /**
    * 自增主键
    */
    private Long id;

    /**
    * 用户id
    */
    private Long userId;

    /**
    * 用户权限:0超级管理员(博客拥有者,所有权限 增删改查),1管理员,2普通用户(由于是个人博客用户只有登录权限没有后台管理权限,而管理员就是博客主人认可的可编辑管理博客的人)
    */
    private Integer authoType;
}