package com.zl.mapper;

import com.zl.entity.po.t_Tag;
import com.zl.entity.po.t_TagExample;

import java.util.List;

import com.zl.entity.povo.Tag;
import org.apache.ibatis.annotations.Param;

public interface t_TagMapper {
    int countByExample(t_TagExample example);

    int deleteByExample(t_TagExample example);

    int deleteByPrimaryKey(Long id);

    int insert(t_Tag record);

    int insertSelective(t_Tag record);

    List<t_Tag> selectByExample(t_TagExample example);

    t_Tag selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") t_Tag record, @Param("example") t_TagExample example);

    int updateByExample(@Param("record") t_Tag record, @Param("example") t_TagExample example);

    int updateByPrimaryKeySelective(t_Tag record);

    int updateByPrimaryKey(t_Tag record);

    /**
     * 通过标签名获取标签信息
     *
     * @param name
     * @return
     */
    t_Tag selectByName(String name);

    /**
     * 获取首页标签页
     *
     * @param size
     * @return
     */
    List<Tag> getTagTop(@Param("size") Integer size);

    /**
     * 通过blogId获取标签
     *
     * @param id
     * @return
     */
    List<Tag> getTagByBlogId(Long id);
}