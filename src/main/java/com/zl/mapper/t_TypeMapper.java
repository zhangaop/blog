package com.zl.mapper;

import com.zl.entity.po.t_Type;
import com.zl.entity.po.t_TypeExample;

import java.util.List;

import com.zl.entity.povo.Blog;
import com.zl.entity.povo.Type;
import org.apache.ibatis.annotations.Param;

public interface t_TypeMapper {
    int countByExample(t_TypeExample example);

    int deleteByExample(t_TypeExample example);

    int deleteByPrimaryKey(Long id);

    int insert(t_Type record);

    int insertSelective(t_Type record);

    List<t_Type> selectByExample(t_TypeExample example);

    t_Type selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") t_Type record, @Param("example") t_TypeExample example);

    int updateByExample(@Param("record") t_Type record, @Param("example") t_TypeExample example);

    int updateByPrimaryKeySelective(t_Type record);

    int updateByPrimaryKey(t_Type record);

    List<t_Type> selectAll();

    /**
     * 通过姓名获取分类信息
     *
     * @param name
     * @return
     */
    t_Type selectByTypeName(@Param("name") String name);

    /**
     * 编辑分类
     * 通过id获取分类
     *
     * @param id
     * @return
     */
    t_Type selectById(Long id);

    List<Type> selectByPage();

    List<Type> getTypeTop(@Param("size") Integer size);

    List<Blog> getBlogByTypeId(Long id);

    Type getTypeById(Long id);
}