package com.zl.mapper;

import com.zl.entity.po.t_Blog_Tags;
import com.zl.entity.po.t_Blog_TagsExample;
import java.util.List;

import com.zl.entity.povo.Blog;
import com.zl.entity.povo.Tag;
import org.apache.ibatis.annotations.Param;

public interface t_Blog_TagsMapper {
    int countByExample(t_Blog_TagsExample example);

    int deleteByExample(t_Blog_TagsExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(t_Blog_Tags record);

    int insertSelective(t_Blog_Tags record);

    List<t_Blog_Tags> selectByExample(t_Blog_TagsExample example);

    t_Blog_Tags selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") t_Blog_Tags record, @Param("example") t_Blog_TagsExample example);

    int updateByExample(@Param("record") t_Blog_Tags record, @Param("example") t_Blog_TagsExample example);

    int updateByPrimaryKeySelective(t_Blog_Tags record);

    int updateByPrimaryKey(t_Blog_Tags record);

    List<Tag> selectTagByUserId(Long id);

    List<Tag> selectByPage();

    /**
     * 通过标签获取对应的博客数量
     * @param id
     * @return
     */
    List<Blog> getBlogByTagId(Long id);
}