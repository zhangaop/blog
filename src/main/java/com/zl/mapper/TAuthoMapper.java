package com.zl.mapper;

import com.zl.entity.po.TAutho;
import org.apache.ibatis.annotations.Param;

/**
 * @author zhanglei2
 * @since 2020/3/22 12:36
 */
public interface TAuthoMapper {
    int deleteByPrimaryKey(Long id);

    int insert(TAutho record);

    int insertSelective(TAutho record);

    TAutho selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(TAutho record);

    int updateByPrimaryKey(TAutho record);

    TAutho selectByUserId(@Param("userId") Long id);
}