package com.zl.mapper;

import com.zl.entity.po.t_Blog;
import com.zl.entity.po.t_BlogExample;

import java.util.List;

import com.zl.entity.povo.Blog;
import com.zl.entity.povo.Type;
import com.zl.entity.povo.User;
import com.zl.vo.BlogQuery;
import org.apache.ibatis.annotations.Param;

public interface t_BlogMapper {
    int countByExample(t_BlogExample example);

    int deleteByExample(t_BlogExample example);

    int deleteByPrimaryKey(Long id);

    long insert(t_Blog record);

    int insertSelective(t_Blog record);

    List<t_Blog> selectByExampleWithBLOBs(t_BlogExample example);

    List<t_Blog> selectByExample(t_BlogExample example);

    t_Blog selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") t_Blog record, @Param("example") t_BlogExample example);

    int updateByExampleWithBLOBs(@Param("record") t_Blog record, @Param("example") t_BlogExample example);

    int updateByExample(@Param("record") t_Blog record, @Param("example") t_BlogExample example);

    int updateByPrimaryKeySelective(t_Blog record);

    int updateByPrimaryKeyWithBLOBs(t_Blog record);

    int updateByPrimaryKey(t_Blog record);

    void deleteTagById(@Param("id") Long id);

    void deleteBlogById(@Param("id") Long id);

    void deleteCommentById(Long id);

    /**
     * 获取博客数量
     *
     * @return
     */
    Long getNum();

    /**
     * 获取user,blog,type相关联的blog信息
     *
     * @param id
     * @return
     */
    Blog selectBlogById(@Param("id") Long id);

    /**
     * 更新浏览次数
     *
     * @param id
     */
    void updateViews(Long id);

    List<Blog> selectAllByAdmin(@Param("user") User user, @Param("blog") BlogQuery blog);

    /**
     * 前台通过标签id获取博客数据
     *
     * @param tagId
     * @return
     */
    List<Blog> selectByTagId(@Param("tagId") Long tagId);

    /**
     * 通过
     *
     * @param tagId
     * @return
     */
    List<Blog> getBlogByTagId(@Param("tagId") Long tagId);

    List<Blog> selectAll();

    List<Blog> selectByQuery(@Param("query") String query);

    List<Blog> selectAllByTypeId(Long id);

    Type getBlogByTypeId(Long id);

    /**
     * 插入博客,返回主键
     * @param t_blog
     * @return
     */
    // Long insertReturnKey(t_Blog t_blog);
}