package com.zl.mapper;

import com.zl.entity.po.t_Comment;
import com.zl.entity.po.t_CommentExample;

import java.util.List;

import com.zl.entity.povo.Comment;
import org.apache.ibatis.annotations.Param;

public interface t_CommentMapper {
    int countByExample(t_CommentExample example);

    int deleteByExample(t_CommentExample example);

    int deleteByPrimaryKey(Long id);

    int insert(t_Comment record);

    int insertSelective(t_Comment record);

    List<t_Comment> selectByExample(t_CommentExample example);

    t_Comment selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") t_Comment record, @Param("example") t_CommentExample example);

    int updateByExample(@Param("record") t_Comment record, @Param("example") t_CommentExample example);

    int updateByPrimaryKeySelective(t_Comment record);

    int updateByPrimaryKey(t_Comment record);

    long selectByBlogId(Long id);

    /**
     * 获取评论及其子平论
     *
     * @return
     */
    // List<Comment> findByBlogIdAndParentCommentNull(@Param("blogId") Long blogId);
}