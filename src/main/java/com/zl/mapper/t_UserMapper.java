package com.zl.mapper;

import com.zl.entity.po.t_User;
import com.zl.entity.po.t_UserExample;
import java.util.List;

import com.zl.entity.vo.userVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
public interface t_UserMapper {
    int countByExample(t_UserExample example);

    int deleteByExample(t_UserExample example);

    int deleteByPrimaryKey(Long id);

    int insert(t_User record);

    int insertSelective(t_User record);

    List<t_User> selectByExample(t_UserExample example);

    t_User selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") t_User record, @Param("example") t_UserExample example);

    int updateByExample(@Param("record") t_User record, @Param("example") t_UserExample example);

    int updateByPrimaryKeySelective(t_User record);

    int updateByPrimaryKey(t_User record);

    t_User selectByUser(@Param("user") t_User user1);

    Integer getNumByUserName(@Param("username") String username);

    /**
     * 通过username+email来判断用户是否存在
     * @param userVo
     * @return
     */
    List<t_User> getNumByUserNameAndEmail(userVo userVo);
}