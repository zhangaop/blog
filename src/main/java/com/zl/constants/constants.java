package com.zl.constants;

/**
 * @author zhanglei2
 * @since 2020/3/10 13:46
 */
public class constants {
    //头像图片静态资源路径
    public static final String HEAD_IMAGER_PATH = "https://unsplash.it/100/100?image=1005";
    //用户权限
    public static final Integer AUTHO_0 = 0;//超级管理员
    public static final Integer AUTHO_1 = 1;//管理员
    public static final Integer AUTHO_2 = 2;//无权限只读用户

    //返回的错误码
    public static final Integer USER_NUM_FAILED = 0;//用户数量异常,大于1
    public static final Integer USER_NUM_EXCEPTION = 1;//用户数量异常,等于0
    public static final Integer USER_FIND_PASSWORD_FAILED = 3;//找回密码失败
    public static final Integer USER_FIND_PASSWORD_SUCCESS = 4;//找回密码成功

}
