package com.zl.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zl.NotFoundException;
import com.zl.dao.TagRepository;
import com.zl.entity.po.t_Blog;
import com.zl.entity.po.t_Tag;
import com.zl.entity.po.t_TagExample;
import com.zl.entity.povo.Blog;
import com.zl.entity.povo.Tag;
import com.zl.mapper.t_BlogMapper;
import com.zl.mapper.t_Blog_TagsMapper;
import com.zl.mapper.t_TagMapper;
import com.zl.service.TagService;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.expression.spel.ast.NullLiteral;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.swing.text.html.parser.TagElement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by limi on 2017/10/16.
 */
@Service
@Log4j
public class TagServiceImpl implements TagService {

    @Autowired
    private TagRepository tagRepository;
    @Autowired
    private t_TagMapper t_tagMapper;
    @Autowired
    private t_Blog_TagsMapper tBlogTagsMapper;
    @Autowired
    private t_BlogMapper blogMapper;

    /**
     * 保存标签
     * @param tag
     * @return
     */
    @Transactional
    @Override
    public Tag saveTag(Tag tag) {
        // return tagRepository.save(tag);
        t_Tag t_tag = new t_Tag();
        BeanUtils.copyProperties(tag,t_tag);
        int i = t_tagMapper.insert(t_tag);
        if(i>0){
            t_Tag t_tag1 = t_tagMapper.selectByName(tag.getName());
            BeanUtils.copyProperties(t_tag1,tag);
            return tag;
        }else {
            return new Tag();
        }
    }

    /**
     * 通过id获取标签信息
     * @param id
     * @return
     */
    @Transactional
    @Override
    public Tag getTag(Long id) {
        // return tagRepository.findOne(id);
        t_Tag t_tag = t_tagMapper.selectByPrimaryKey(id);
        Tag tag =new Tag();
        BeanUtils.copyProperties(t_tag,tag);
        return tag;
    }

    /**
     * 通过标签名获取标签
     * @param name
     * @return
     */
    @Override
    public Tag getTagByName(String name) {
        // return tagRepository.findByName(name);
        t_Tag t_tag = t_tagMapper.selectByName(name);
        if(t_tag==null){
            return null;
        }
        Tag tag = new Tag();
        BeanUtils.copyProperties(t_tag,tag);
        return tag;
    }

    /**
     * 分页查看标签数据
     * @param pageNum
     * @return
     */
    @Transactional
    @Override
    public PageInfo<Tag> listTag(Integer pageNum) {
        PageHelper.startPage(pageNum,8);
        List<Tag> list = tBlogTagsMapper.selectByPage();
        PageInfo<Tag> pageInfo = new PageInfo<>(list);
        return pageInfo;
    }

    /**
     * 获取所有标签
     * @return
     */
    @Override
    public List<Tag> listTag() {
        // return tagRepository.findAll();
        t_TagExample t_tagExample = new t_TagExample();
        List<t_Tag> t_tags = t_tagMapper.selectByExample(t_tagExample);
        List<Tag> list = new ArrayList<>();
        for (t_Tag t_tag : t_tags) {
            Tag tag = new Tag();
            BeanUtils.copyProperties(t_tag,tag);
            list.add(tag);
        }
        return list;
    }

    @Override
    public List<Tag> listTagTop(Integer size) {
        /*log.info("============listTagTop===========");
        Sort sort = new Sort(Sort.Direction.DESC, "blogs.size");
        Pageable pageable = new PageRequest(0, size, sort);

        log.info("============listTagTop===========");*/
        List<Tag> tags = t_tagMapper.getTagTop(size);
        for (Tag tag : tags) {
            List<Blog> blogs = tBlogTagsMapper.getBlogByTagId(tag.getId());
            tag.setBlogs(blogs);
        }
        return tags;
    }

    /**
     * 分割tagId的字符串获取所有标签数据
     * @param ids
     * @return
     */
    @Override
    public List<Tag> listTag(String ids) { //1,2,3
        // return tagRepository.findAll(convertToList(ids));
        List<Long> longs = convertToList(ids);
        List<Tag> tagList = new ArrayList<>();
        for (Long aLong : longs) {
            Tag tag = new Tag();
            t_Tag t_tag = t_tagMapper.selectByPrimaryKey(aLong);
            BeanUtils.copyProperties(t_tag,tag);
            tagList.add(tag);
        }
        return tagList;
    }

    private List<Long> convertToList(String ids) {
        List<Long> list = new ArrayList<>();
        if (!"".equals(ids) && ids != null) {
            String[] idarray = ids.split(",");
            for (int i=0; i < idarray.length;i++) {
                list.add(new Long(idarray[i]));
            }
        }
        return list;
    }

    /**
     * 更新标签
     * @param id
     * @param tag
     * @return
     */
    @Transactional
    @Override
    public Tag updateTag(Long id, Tag tag) {
        // Tag t = tagRepository.findOne(id);
        /**
         * 通过id获取标签
         */
        t_Tag t_tag = t_tagMapper.selectByPrimaryKey(id);
        if (t_tag == null) {
            throw new NotFoundException("不存在该标签");
        }
        BeanUtils.copyProperties(tag,t_tag);
        // return tagRepository.save(t);
        int i = t_tagMapper.updateByPrimaryKeySelective(t_tag);
        if(i>0){
            // t_Tag t_tag1 = t_tagMapper.selectByPrimaryKey(id);
            // BeanUtils.copyProperties();
            return tag;
        }else {
            return new Tag();
        }
    }


    /**
     * 删除标签
     * @param id
     */
    @Transactional
    @Override
    public void deleteTag(Long id) {
        // tagRepository.delete(id);
        t_tagMapper.deleteByPrimaryKey(id);
    }
}
