package com.zl.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zl.NotFoundException;
import com.zl.dao.TypeRepository;
import com.zl.entity.po.t_Type;
import com.zl.entity.povo.Blog;
import com.zl.entity.povo.Tag;
import com.zl.entity.povo.Type;
import com.zl.mapper.t_BlogMapper;
import com.zl.mapper.t_TypeMapper;
import com.zl.service.TypeService;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by limi on 2017/10/16.
 */
@Service
@Log4j
public class TypeServiceImpl implements TypeService {

    @Autowired
    private TypeRepository typeRepository;
    @Autowired
    private t_TypeMapper t_typeMapper;
    @Autowired
    private t_BlogMapper blogMapper;

    /**
     * 保存分类type
     * @param type
     * @return
     */
    @Transactional
    @Override
    public Type saveType(Type type) {
        t_Type t_type = new t_Type();
        BeanUtils.copyProperties(type,t_type);
        int i = t_typeMapper.insertSelective(t_type);
        if(i>0){
           t_Type t_type1 = t_typeMapper.selectByTypeName(type.getName());
           BeanUtils.copyProperties(t_type1,type);
            return type;
        }else{
            return new Type();
        }
        // return typeRepository.save(type);
    }

    /**
     * 通过id获取分类
     * @param id
     * @return
     */
    @Transactional
    @Override
    public Type getType(Long id) {
        // return typeRepository.findOne(id);
        // t_Type t_type = t_typeMapper.selectById(id);
        Type type = t_typeMapper.getTypeById(id);
        // Type type = new Type();
        // BeanUtils.copyProperties(t_type,type);
        return type;
    }

    /**
     * 通过分类名称获取分类数据
     * @param name
     * @return
     */
    @Override
    public Type getTypeByName(String name) {
        t_Type t_type = t_typeMapper.selectByTypeName(name);
        if(t_type==null){
            return null;
        }
        Type type = new Type();
        BeanUtils.copyProperties(t_type,type);
        return type;
        // return typeRepository.findByName(name);
    }

    @Transactional
    @Override
    public PageInfo<Type> listType(Integer pageNum) {
        PageHelper.startPage(pageNum,8);
        List<Type> types = t_typeMapper.selectByPage();
        PageInfo<Type> pageInfo = new PageInfo<>(types);
        return pageInfo;
    }

    /**
     * 获取所有分类列表
     * @return
     */
    @Override
    public List<Type> listType() {
        log.info("获取所有的分类");
        List<t_Type> t_types = t_typeMapper.selectAll();
        List<Type> types = new ArrayList<>();
        for (t_Type type : t_types) {
            Type type1 = new Type();
            BeanUtils.copyProperties(type,type1);
            types.add(type1);
        }
        return types;
        // return typeRepository.findAll();
    }


    @Override
    public List<Type> listTypeTop(Integer size) {
        /*Sort sort = new Sort(Sort.Direction.DESC,"blogs.size");
        Pageable pageable = new PageRequest(0,size,sort);
        return typeRepository.findTop(pageable);*/
        List<Type> types =  t_typeMapper.getTypeTop(size);
        for (Type type : types) {
            List<Blog> blogs = t_typeMapper.getBlogByTypeId(type.getId());
            type.setBlogs(blogs);
        }
        return types;
    }

    /**
     * 编辑更新分类数据
     * @param id
     * @param type
     * @return
     */
    @Transactional
    @Override
    public Type updateType(Long id, Type type) {
        // Type t = typeRepository.findOne(id);
        t_Type t_type = t_typeMapper.selectById(id);
        if (t_type == null) {
            throw new NotFoundException("不存在该类型");
        }
        BeanUtils.copyProperties(type,t_type);
        // return typeRepository.save(t);
        int i = t_typeMapper.updateByPrimaryKey(t_type);
        if(i>0){
            t_Type t_type1 = t_typeMapper.selectById(type.getId());
            BeanUtils.copyProperties(t_type1,type);
            return type;
        }else{
            return new Type();
        }
    }

    /**
     * 删除分类
     * @param id
     */
    @Transactional
    @Override
    public void deleteType(Long id) {
        // typeRepository.delete(id);
        t_typeMapper.deleteByPrimaryKey(id);
    }
}
