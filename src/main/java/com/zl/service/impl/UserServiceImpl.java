package com.zl.service.impl;

import com.zl.NotFoundException;
import com.zl.constants.constants;
import com.zl.dao.UserRepository;
import com.zl.entity.po.TAutho;
import com.zl.entity.vo.userVo;
import com.zl.mapper.TAuthoMapper;
import com.zl.mapper.t_UserMapper;
import com.zl.entity.po.t_User;
import com.zl.service.UserService;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;


/**
 * @author zhanglei2
 * @date
 */
@Transactional
@Service("userService")
@Log4j
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private t_UserMapper userMapper;
    @Autowired
    private TAuthoMapper authoMapper;
    @Override
    public t_User checkUser(String username, String password) {
        t_User user1 = new t_User();
        user1.setUsername(username);
        user1.setPassword(password);
        t_User user = userMapper.selectByUser(user1);
        return user;
    }

    /**
     * 注册用户
     * @param user
     * @return
     */
    @Override
    public t_User regUser(t_User user) {
        user.setAvatar(constants.HEAD_IMAGER_PATH);
        user.setCreateTime(new Date());
        user.setUpdateTime(new Date());
        try {
            int idKey = userMapper.insertSelective(user);
            TAutho tAutho = new TAutho();
            tAutho.setUserId((long) idKey);
            tAutho.setAuthoType(constants.AUTHO_2);
            authoMapper.insertSelective(tAutho);
        } catch (Exception e) {
            log.error("注册失败{}",e);
            return null;
        }
        return user;
    }

    @Override
    public Integer getNumByUserName(String username) {
        return userMapper.getNumByUserName(username);
    }

    /**
     * 通过username+email来判断用户是否存在
     * @param userVo
     * @return
     */
    @Override
    public List<t_User> getNumByUserNameAndEmail(userVo userVo) {
        return userMapper.getNumByUserNameAndEmail(userVo);
    }
}
