package com.zl.service;

import com.github.pagehelper.PageInfo;
import com.zl.entity.povo.Blog;
import com.zl.entity.povo.User;
import com.zl.vo.BlogQuery;
import org.omg.CORBA.INTERNAL;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * Created by limi on 2017/10/20.
 */
public interface BlogService {

    Blog getBlog(Long id);

    Blog getAndConvert(Long id);

    /**
     * 后台分页获取博客列表
     * @param pageNum
     * @param type
     * @return
     */
    PageInfo<Blog> listBlog(Integer pageNum,Long type );

    PageInfo<Blog> listBlog(Integer pageNum);

    PageInfo<Blog> listBlog(Long tagId,Integer pageNum);

    PageInfo<Blog> listBlog(String query,Integer pageNum);

    List<Blog> listRecommendBlogTop(Integer size);

    Map<String,List<Blog>> archiveBlog();

    Long countBlog();

    Blog saveBlog(Blog blog);

    Blog updateBlog(Long id,Blog blog);

    /**
     * mybatis后台删除博客文章
     * @param id
     */
    void deleteBlog(Long id);

    PageInfo<Blog> listBlog(Integer pageNum, BlogQuery blog, User user);
}
