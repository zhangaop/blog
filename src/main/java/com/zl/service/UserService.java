package com.zl.service;

import com.zl.entity.po.t_User;
import com.zl.entity.vo.userVo;

import java.util.List;

/**
 * Created by limi on 2017/10/15.
 */
public interface UserService {

    t_User checkUser(String username, String password);

    /**
     * 注册用户
     *
     * @param t_user
     * @return
     */
    t_User regUser(t_User t_user);

    /**
     * 判断用户名是否已注册
     * @param username
     * @return
     */
    Integer getNumByUserName(String username);

    /**
     * 通过username+email来判断用户是否存在
     * @param userVo
     * @return
     */
    List<t_User> getNumByUserNameAndEmail(userVo userVo);
}
