package com.zl;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;

/**
 * @author zhanglei2
 * @since 2020/3/13 16:24
 */
public class TestVo {
    private String dsusername;
    private String dpassword;
    private String dsemail;

}
